# Aplikasi API Gateway #

* Enable metrics di aplikasi Spring Boot : https://docs.spring.io/spring-boot/docs/2.0.3.RELEASE/reference/htmlsingle/#production-ready-endpoints-exposing-endpoints
* Enable metrics di API Gateway : http://cloud.spring.io/spring-cloud-static/Finchley.SR1/single/spring-cloud.html#_gateway_metrics_filter

* URL Metrics :

    * `/actuator/metrics/gateway.requests` : statistik akses ke gateway
    * `/actuator/prometheus` : statistik aplikasi dalam format Prometheus
